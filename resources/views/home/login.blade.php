@extends('master')
@section('content')
  <div class="container login">
    <div class="title">
      <span>Saznoor Industries Inventory System</span>
      <hr>
    </div>
    <div class="login-form">
      <form action="{{ action('HomeController@login_check') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
          <div class="input-group mb-2">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-user"></i>
              </div>
            </div>
            <input name="username" type="text" class="form-control" placeholder="Username">
          </div>
        </div>
        <div class="form-group">
          <div class="input-group mb-2">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-lock"></i>
              </div>
            </div>
            <input name="password" type="password" class="form-control" placeholder="Password">
          </div>
        </div>
        <div class="button-div">
          <button type="submit" name="login" class="btn btn-outline-dark">Log in</button>
        </div>
        @if ($errors->any())
          <div class="alert alert-danger" role="alert" style="margin-top: 20px;">
            <br>
            @foreach ($errors->all() as $e)
              <p>{{$e}}</p>
            @endforeach
          </div>
        @endif
      </form>
    </div>
  </div>
@endsection
