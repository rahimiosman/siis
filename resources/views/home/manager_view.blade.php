<div class="home-content">
  <div class="location-title">
    <div class="dropdown">
      <button class="btn btn-lg btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{Helpers::neat($location[0]->clinic_location)}}
      </button>
      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        @foreach ($location as $l)
          <a class="dropdown-item" href="#">{{Helpers::neat($l->clinic_location)}}</a>
        @endforeach
      </div>
    </div>
  </div>
  <script type="text/javascript">
    var chart = new Array();
    var chartData = new Array();
    var chartID = new Array();
  </script>
  @foreach ($location as $lo)
    @if ($loop->index == 0)
      <div class="row" id="{{$lo->clinic_location}}">
    @else
      <div class="row" id="{{$lo->clinic_location}}" style="display: none;">
    @endif

    @foreach ($products as $product)
      @if ($product->product_location == $lo->clinic_location)
        <div class="col-4">
          <div class="chart-box">
            <canvas id="{{'chart'.$product->product_name.$loop->index}}" width="200" height="200"></canvas>
            <div class="item-name">
              <span>{{Helpers::neat($product->product_name)}}</span>
            </div>
          </div>
        </div>
        <script type="text/javascript">
          chartID.push(document.getElementById("{{'chart'.$product->product_name.$loop->index}}").getContext('2d'));
          chartData.push(
            {
              type: 'doughnut',
              data: {
                labels: ["Current Stock", "Max Stock"],
                datasets: [{
                    data: [{{$product->product_current_quantity}}, {{$product->product_max_quantity}}],
                    backgroundColor: [
                        '#c51162',
                        '#02bfe0'
                    ]
                }],
                borderColor: [
                  '#000000',
                  '#000000'
                ]
              },
              options: {
                responsive: false
              }
            }
          );
          chart.push(new Chart(document.getElementById("{{'chart'.$product->product_name.$loop->index}}").getContext('2d'), {
            type: 'doughnut',
            data: {
              labels: ["Current Stock", "Max Stock"],
              datasets: [{
                  data: [{{$product->product_current_quantity}}, {{$product->product_max_quantity}}],
                  backgroundColor: [
                      '#c51162',
                      '#02bfe0'
                  ]
              }],
              borderColor: [
                '#000000',
                '#000000'
              ]
            },
            options: {
              responsive: false
            }
          })
        );
        </script>
      @endif
    @endforeach
    </div>
  @endforeach
</div>
<script type="text/javascript">
  var temp = '#' + "{{$location[0]->clinic_location}}";

  $(".dropdown-menu a").click(function(){
    $(temp).hide();
    resetChart();
    var text = $(this).text().toLowerCase().replace(" ", "-");
    temp = '#' + text;
    $(this).parents(".dropdown").find('.btn').html($(this).text());
    $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
    $('#'+text).show();
  });

  function resetChart() {
    for (var i = 0; i < chart.length; i++) {
      chart[i].destroy();
      chart[i] = new Chart(chartID[i], chartData[i]);
    }
  }
</script>
