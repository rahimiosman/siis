@extends('master')
@section('content')
  <div class="inbox-content">
    <div class="title">
      <span>Inbox</span>
      <hr>
    </div>
    <div class="inbox-table">
      <table class="table table-hover table-dark table-bordered">
        <thead>
          <tr style="text-align: center;">
            <th scope="col">Messages/Request</th>
            <th scope="col" style="width: 14em;">Time/Date</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($inboxes as $inbox)
            @if ($inbox->read_status)
              <tr>
                <td>
                  <span>Product Name: {{Helpers::neat($inbox->product_name)}}</span>
                  <br>
                  <span>Type: {{Helpers::neat($inbox->type)}}</span>
                  <br>
                  <span>Quantity: {{$inbox->quantity}}</span>
                  <br>
                  <span>Location: {{Helpers::neat($inbox->location)}}</span>
                  <br>
                  <span>Message: {{$inbox->message}}</span>
                </td>
                <td style="text-align: center; vertical-align: middle;">{{$inbox->created_at}}</td>
              </tr>
            @else
              <tr class="unread">
                <td>
                  <a href="/update-inbox/{{$inbox->id}}">
                    <span>Product Name: {{Helpers::neat($inbox->product_name)}}</span>
                    <br>
                    <span>Type: {{Helpers::neat($inbox->type)}}</span>
                    <br>
                    <span>Quantity: {{$inbox->quantity}}</span>
                    <br>
                    <span>Location: {{Helpers::neat($inbox->location)}}</span>
                    <br>
                    <span>Message: {{$inbox->message}}</span>
                  </a>
                </td>
                <td style="text-align: center; vertical-align: middle;">{{$inbox->created_at}}</td>
              </tr>
            @endif
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection
