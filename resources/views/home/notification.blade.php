@extends('master')
@section('content')
  <div class="notification-content">
    <div class="title">
      <span>Notification</span>
      <hr>
    </div>
    <div class="inbox-table">
      <table class="table table-hover table-dark table-bordered">
        <thead>
          <tr style="text-align: center;">
            <th scope="col">Notification</th>
            <th scope="col" style="width: 14em;">Time/Date</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($notifications as $notification)
            @if ($notification->read_status)
              <tr>
                <td>
                  <span>{{Helpers::neat($notification->product_name)}} (Type: {{Helpers::neat($notification->product_type)}}) has reached low amount at {{Helpers::neat($notification->location)}}</span>
                  <br>
                  <span>Current Quantity: {{$notification->current_quantity}}</span>
                  <br>
                  <span>Max Quantity: {{$notification->max_quantity}}</span>
                </td>
                <td style="text-align: center; vertical-align: middle;">{{$notification->created_at}}</td>
              </tr>
            @else
              <tr class="unread">
                <td>
                  <a href="/update-notification/{{$notification->id}}">
                    <span>{{Helpers::neat($notification->product_name)}} (Type: {{Helpers::neat($notification->product_type)}}) has reached low amount at {{Helpers::neat($notification->location)}}</span>
                    <br>
                    <span>Current Quantity: {{$notification->current_quantity}}</span>
                    <br>
                    <span>Max Quantity: {{$notification->max_quantity}}</span>
                  </a>
                </td>
                <td style="text-align: center; vertical-align: middle;">{{$notification->created_at}}</td>
              </tr>
            @endif
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection
