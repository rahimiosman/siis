@extends('master')
@section('content')
  @if (session('position') == 'manager')
    @include('home.manager_view')
  @else
    @include('home.staff_view')
  @endif
@endsection
