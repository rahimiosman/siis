<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="{{asset('/css/app.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="icon" href="images/siz.jpg"/>
    <script src="{{asset('/js/app.js')}}"></script>
    <title>SIIS</title>
  </head>
  @if (url()->current() != action('HomeController@login'))
    @include('shared.nav')
    <body>
      <div class="container-fluid">
        <div class="row">
          @include('shared.dashboard')
          <div class="col-10 default-content-style">
            @yield('content')
          </div>

        </div>
      </div>
  @else
    <body class="login-background" id="app">
      @yield('content')
  @endif
  </body>
</html>
