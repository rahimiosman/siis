<!-- Modal Update -->
<div class="modal fade" id="{{ 'u'.$staff->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="{{ action('ManagerController@update_staff') }}">
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="form-group">
            <label>Name</label>
            <input type="text" name="old-name" value="{{ $staff->fullname }}" hidden>
            <input type="text" name="old-phone-number" value="{{ $staff->phone_number }}" hidden>
            <input type="text" name="old-location" value="{{ $staff->location }}" hidden>
            <input name="name" class="form-control" value="{{ Helpers::neat($staff->fullname) }}"/>
          </div>
          <div class="form-group">
            <label>Phone Number</label>
            <input name="phone-number" class="form-control" value="{{ $staff->phone_number }}"/>
          </div>
          <div class="form-group">
            <label>Address</label>
            <input name="address" class="form-control" value="{{ $staff->address }}"/>
          </div>
          <div class="form-group">
            <label>Position</label>
            <input name="position" class="form-control" value="{{ Helpers::neat($staff->position) }}"/>
          </div>
          <div class="form-group">
            <label>Location</label>
            <input name="location" class="form-control" value="{{ Helpers::neat($staff->location) }}"/>
          </div>
          <div class="form-group">
            <label>Username</label>
            <input name="username" class="form-control" value="{{ $staff->username }}"/>
          </div>
          <div class="form-group">
            <label>Password</label>
            <input name="password" class="form-control" value="{{ $staff->password }}"/>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
