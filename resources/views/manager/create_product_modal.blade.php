<!-- Modal Update -->
<div class="modal fade" id="create-product" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="{{ action('ManagerController@create_product') }}">
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="form-group">
            <label>Product Name:</label>
            <input class="form-control" name="product-name"/>
          </div>
          <div class="form-group">
            <label>Product Type:</label>
            <input name="product-type" class="form-control"/>
          </div>
          <div class="form-group">
            <label>Product Location:</label>
            <input name="product-location" class="form-control"/>
          </div>
          <div class="form-group">
            <label>Product Max Quantity:</label>
            <input name="product-max-quantity" class="form-control"/>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Create</button>
        </div>
      </form>
    </div>
  </div>
</div>
