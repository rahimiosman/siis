@extends('master')
@section('content')
  <div class="staff-management-content">
    <div class="title">
      <span>Staff Management</span>
      <hr>
    </div>
    <div class="add-new-staff">
      <a href="#" data-toggle="modal" data-target="#create-staff">
        <i class="fas fa-plus-square"></i>
        <span>Add new staff</span>
      </a>
    </div>
    @if ($errors->any())
      @if ($errors->first('success'))
        <div class="alert alert-success">
          {{ $errors->first('success') }}
        </div>
      @else
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $e)
              <li>{{ $e }}</li>
            @endforeach
          </ul>
        </div>
      @endif
    @endif
    @include('manager.create_staff_modal')
    <div>
      <table class="table table-hover table-dark table-bordered">
        <thead>
          <tr style="text-align: center;">
            <th scope="col">No.</th>
            <th scope="col">Name</th>
            <th scope="col">Phone Number</th>
            <th scope="col">Address</th>
            <th scope="col">Position</th>
            <th scope="col">Location</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody style="text-align: center;">
          @foreach ($staffs as $staff)
            <tr>
              <td>{{ $staff->id }}</td>
              <td>{{ Helpers::neat($staff->fullname) }}</td>
              <td>{{ $staff->phone_number }}</td>
              <td>{{ $staff->address }}</td>
              <td>{{ Helpers::neat($staff->position) }}</td>
              <td>{{ Helpers::neat($staff->location) }}</td>
              <td>
                <a href="#" class="btn btn-outline-light" data-toggle="modal" data-target="{{ '#u'.$staff->id }}">Update</a>
                <a style="margin-left: 10px;" href="#" class="btn btn-outline-danger" data-toggle="modal" data-target="{{ '#d'.$staff->id }}">Delete</a>
              </td>
              @include('manager.update_modal')
              @include('manager.delete_modal')
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection
