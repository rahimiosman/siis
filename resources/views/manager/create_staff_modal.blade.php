<!-- Modal Update -->
<div class="modal fade" id="create-staff" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="{{ action('ManagerController@create_staff') }}">
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="form-group">
            <label>Name</label>
            <input name="name" class="form-control"/>
          </div>
          <div class="form-group">
            <label>Phone Number</label>
            <input name="phone-number" class="form-control"/>
          </div>
          <div class="form-group">
            <label>Address</label>
            <input name="address" class="form-control"/>
          </div>
          <div class="form-group">
            <label>Position</label>
            <input name="position" class="form-control"/>
          </div>
          <div class="form-group">
            <label>Location</label>
            <input name="location" class="form-control"/>
          </div>
          <div class="form-group">
            <label>Username</label>
            <input name="username" class="form-control"/>
          </div>
          <div class="form-group">
            <label>Password</label>
            <input name="password" class="form-control"/>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Create</button>
        </div>
      </form>
    </div>
  </div>
</div>
