@extends('master')
@section('content')
  <div class="inventory-management-content">
    <div class="title">
      <span>Inventory Management</span>
      <hr>
    </div>
    @if ($errors->any())
      @if ($errors->first('success'))
        <div class="alert alert-success">
          {{ $errors->first('success') }}
        </div>
      @else
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $e)
              <li>{{ $e }}</li>
            @endforeach
          </ul>
        </div>
      @endif
    @endif
    <div class="add-new-product">
      <a href="#" data-toggle="modal" data-target="#create-product">
        <i class="fas fa-plus-square"></i>
        <span>Add new product</span>
      </a>
    </div>
    <div>
      <table class="table table-hover table-dark table-bordered">
        <thead>
          <tr style="text-align: center;">
            <th scope="col">No.</th>
            <th scope="col">Product Name</th>
            <th scope="col">Type</th>
            <th scope="col">Max Quantity</th>
            <th scope="col">Current Quantity</th>
            <th scope="col">Location</th>
          </tr>
        </thead>
        <tbody style="text-align: center;">
          @foreach ($products as $product)
            <tr>
              <td>{{ $product->id }}</td>
              <td>{{ Helpers::neat($product->product_name) }}</td>
              <td>{{ Helpers::neat($product->product_type) }}</td>
              <td>{{ $product->product_max_quantity }}</td>
              <td>{{ $product->product_current_quantity }}</td>
              <td>{{ Helpers::neat($product->product_location) }}</td>
            <tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  @include('manager.create_product_modal')
@endsection
