<!-- Modal Delete -->
<div class="modal fade" id="{{ 'd'.$staff->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Remove {{ Helpers::neat($staff->fullname) }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <strong>Do you really wish to remove this device?</strong>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
        <a href="{{ '/delete-staff/'.$staff->id.'/'.Helpers::raw($staff->fullname).'/'.Helpers::raw($staff->location) }}" class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('delete-staff-form-{{ $staff->id }}').submit();">Yes</a>
        <form style="display: none;" id="delete-staff-form-{{ $staff->id }}" action="{{ '/delete-staff/'.$staff->id.'/'.Helpers::raw($staff->fullname).'/'.Helpers::raw($staff->location) }}" method="post">
          {{ csrf_field() }}
          {{ method_field('delete') }}
        </form>
      </div>
    </div>
  </div>
</div>
