@extends('master')
@section('content')
  <div class="submit-request">
    <div class="title">
      <span>Submit Request</span>
      <hr>
    </div>
    @if ($errors->any())
      @if ($errors->first('success'))
        <div class="alert alert-success">
          {{ $errors->first('success') }}
        </div>
      @else
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $e)
              <li>{{ $e }}</li>
            @endforeach
          </ul>
        </div>
      @endif
    @endif
    <div class="content">
      <div class="selection-button">
        <button onclick="requestStock()" class="btn btn-outline-light" type="button" name="button">Request Stock</button>
        <button onclick="requestNewStock()" class="btn btn-outline-info secondary" type="button" name="button">Request New Product</button>
      </div>
      <div id="loader" class="loader">
        <i class="fa fa-spinner fa-spin" style="font-size: 3rem;"></i>
        <div style="margin-top: 10px;">
          Loading...
        </div>
      </div>
      <form id="request-stock" style="display: none;" action="{{ action('StaffController@request_stock') }}" method="post">
        {{ csrf_field() }}
        <div class="title">
          <span>Request Stock</span>
        </div>
        <div class="form-group">
          <label>Product Name</label>
          <select name="product-name" class="form-control">
            @foreach ($products as $product)
              <option>{{ Helpers::neat($product->product_name) }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label>Message</label>
          <textarea class="form-control" name="message" rows="8" cols="80"></textarea>
        </div>
        <div class="form-group">
          <label>Quantity</label>
          <input type="text" name="quantity" class="form-control" style="width: 5em;">
        </div>
        <div class="button-div">
          <button type="submit" class="btn btn-outline-primary">Submit</button>
        </div>
      </form>
      <form id="request-new-stock" style="display: none;" action="{{ action('StaffController@request_new_product') }}" method="post">
        {{ csrf_field() }}
        <div class="title">
          <span>Request New Product</span>
        </div>
        <div class="form-group">
          <label>Product Name</label>
          <input type="text" name="product-name" class="form-control">
        </div>
        <div class="form-group">
          <label>Message</label>
          <textarea class="form-control" name="message" rows="8" cols="80"></textarea>
        </div>
        <div class="form-group">
          <label>Quantity</label>
          <input type="text" name="quantity" class="form-control" style="width: 5em;">
        </div>
        <div class="button-div">
          <button type="submit" class="btn btn-outline-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
  <script type="text/javascript">
    function requestStock() {
      $('#request-new-stock').hide();
      $('#request-stock').hide();
      $('#loader').show();

      setTimeout(function () {
          $('#loader').hide();
          $('#request-stock').show();
      }, 800);
    }

    function requestNewStock() {
      $('#request-stock').hide();
      $('#request-new-stock').hide();
      $('#loader').show();

      setTimeout(function () {
          $('#loader').hide();
          $('#request-new-stock').show();
      }, 800);

    }
  </script>
@endsection
