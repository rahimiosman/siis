@extends('master')
@section('content')
  <div class="release-restock">
    <div class="title">
      <span>Restock Product</span>
      <hr>
    </div>
    <div class="content">
      <form action="{{ action('StaffController@restock') }}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
          <label>Product Name</label>
          <select name="product-name" class="form-control">
            @foreach ($products as $product)
              <option>{{ Helpers::neat($product->product_name) }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label>Product Type</label>
          <select name="product-type" class="form-control" style="width: 20em;">
            @foreach ($products as $product)
              <option>{{ Helpers::neat($product->product_type) }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label>Quantity</label>
          <input class="form-control" name="quantity" style="width: 5em;">
        </div>
        <div class="button-div">
          <button type="submit" class="btn btn-outline-info" name="button">Restock</button>
        </div>
      </form>
    </div>
  </div>
@endsection
