@extends('master')
@section('content')
  <div class="staff-management-content">
    <div class="title">
      <span>Staff List</span>
      <hr>
    </div>
    <div>
      <table class="table table-hover table-dark table-bordered">
        <thead>
          <tr style="text-align: center;">
            <th scope="col">No.</th>
            <th scope="col">Name</th>
            <th scope="col">Phone Number</th>
            <th scope="col">Address</th>
            <th scope="col">Position</th>
          </tr>
        </thead>
        <tbody style="text-align: center;">
          @foreach ($staff_list as $sl)
            <tr>
              <td>{{ $sl->id }}</td>
              <td>{{ Helpers::neat($sl->fullname) }}</td>
              <td>{{ $sl->phone_number }}</td>
              <td>{{ Helpers::neat($sl->address) }}</td>
              <td>{{ Helpers::neat($sl->position) }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection
