<nav class="navbar navbar-dark bg-dark custom-navbar">
  <a class="navbar-brand custom-title" href="/">Saznoor Industries</a>
  <div>
    <ul class="navbar-nav">
      <li class="nav-item ">
        <a class="nav-link" href="{{ action('HomeController@logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
        <form id="logout-form" method="post" action="{{ action('HomeController@logout') }}" style="display: none;">
          {{ csrf_field() }}
        </form>
      </li>
    </ul>
  </div>
</nav>
