<div class="col-2 dashboard-nav">
  <div class="list-group custom-list">
    <li class="list-group-item custom-title">
      @if (session('position') == 'manager')
        <span class="manager">{{ Helpers::neat(session('position')) }}</span>
      @else
        <span>{{ Helpers::neat(session('position')).' ('.Helpers::neat(session('location')).')' }}</span>
      @endif
    </li>
    @if (session('position') == 'manager')
      @if (url()->current() == action('HomeController@inbox'))
        <a href="{{ action('HomeController@inbox') }}" class="list-group-item list-group-item-action list-group-item-secondary active">
      @else
        <a href="{{ action('HomeController@inbox') }}" class="list-group-item list-group-item-action list-group-item-secondary">
      @endif
        <div class="row">
          <div class="col-2">
            <i class="fas fa-envelope"></i>
          </div>
          <div class="col" style="margin-right: 10px;">
            Inbox
          </div>
          <div class="col" style="text-align: right; font-family: serif;">
            <span>{{ App\Inbox::get_inbox_value() }}</span>
          </div>
        </div>
      </a>
      @if (url()->current() == action('HomeController@notification'))
        <a href="{{ action('HomeController@notification') }}" class="list-group-item list-group-item-action list-group-item-secondary active">
      @else
        <a href="{{ action('HomeController@notification') }}" class="list-group-item list-group-item-action list-group-item-secondary">
      @endif
        <div class="row">
          <div class="col-2">
            <i class="fas fa-bell"></i>
          </div>
          <div class="col" style="margin-right: 10px;">
            Notification
          </div>
          <div class="col" style="text-align: right; font-family: serif;">
            <span id="notification-value"></span>
          </div>
        </div>
      </a>
      @if (url()->current() == action('ManagerController@staff_management'))
        <a href="{{ action('ManagerController@staff_management') }}" class="list-group-item list-group-item-action list-group-item-secondary active">
      @else
        <a href="{{ action('ManagerController@staff_management') }}" class="list-group-item list-group-item-action list-group-item-secondary">
      @endif
        <div class="row">
          <div class="col-2">
            <i class="fas fa-users"></i>
          </div>
          <div class="col" style="margin-right: 10px;">
            Staff Management
          </div>
        </div>
      </a>
      @if (url()->current() == action('ManagerController@inventory_management'))
        <a href="{{ action('ManagerController@inventory_management') }}" class="list-group-item list-group-item-action list-group-item-secondary active">
      @else
        <a href="{{ action('ManagerController@inventory_management') }}" class="list-group-item list-group-item-action list-group-item-secondary">
      @endif
        <div class="row">
          <div class="col-2" style="padding-top: 0.75em;">
            <i class="fas fa-box-open"></i>
          </div>
          <div class="col" style="margin-right: 10px;">
            Inventory Management
          </div>
        </div>
      </a>
    @elseif (session('position') == 'clerk')
      @if (url()->current() == action('StaffController@inventory_list'))
        <a href="{{ action('StaffController@inventory_list') }}" class="list-group-item list-group-item-action list-group-item-secondary active">
      @else
        <a href="{{ action('StaffController@inventory_list') }}" class="list-group-item list-group-item-action list-group-item-secondary">
      @endif
        <div class="row">
          <div class="col-2">
            <i class="fas fa-box"></i>
          </div>
          <div class="col" style="margin-right: 10px;">
            Inventory List
          </div>
        </div>
      </a>
      @if (url()->current() == action('StaffController@staff_list'))
        <a href="{{ action('StaffController@staff_list') }}" class="list-group-item list-group-item-action list-group-item-secondary active">
      @else
        <a href="{{ action('StaffController@staff_list') }}" class="list-group-item list-group-item-action list-group-item-secondary">
      @endif
        <div class="row">
          <div class="col-2">
            <i class="fas fa-users"></i>
          </div>
          <div class="col" style="margin-right: 10px;">
            Staff List
          </div>
        </div>
      </a>
      @if (url()->current() == action('StaffController@submit_request'))
        <a href="{{ action('StaffController@submit_request') }}" class="list-group-item list-group-item-action list-group-item-secondary active">
      @else
        <a href="{{ action('StaffController@submit_request') }}" class="list-group-item list-group-item-action list-group-item-secondary">
      @endif
        <div class="row">
          <div class="col-2">
            <i class="fas fa-envelope"></i>
          </div>
          <div class="col" style="margin-right: 10px;">
            Submit Request
          </div>
        </div>
      </a>
      @if (url()->current() == action('StaffController@release_product'))
        <a href="{{ action('StaffController@release_product') }}" class="list-group-item list-group-item-action list-group-item-secondary active">
      @else
        <a href="{{ action('StaffController@release_product') }}" class="list-group-item list-group-item-action list-group-item-secondary">
      @endif
        <div class="row">
          <div class="col-2">
            <i class="fas fa-share-square"></i>
          </div>
          <div class="col" style="margin-right: 10px;">
            Release Product
          </div>
        </div>
      </a>
      @if (url()->current() == action('StaffController@restock_product'))
        <a href="{{ action('StaffController@restock_product') }}" class="list-group-item list-group-item-action list-group-item-secondary active">
      @else
        <a href="{{ action('StaffController@restock_product') }}" class="list-group-item list-group-item-action list-group-item-secondary">
      @endif
        <div class="row">
          <div class="col-2">
            <i class="fas fa-cubes"></i>
          </div>
          <div class="col" style="margin-right: 10px;">
            Restock Product
          </div>
        </div>
      </a>
    @endif

  </div>
</div>
@if (session('position') == 'manager')
  <script type="text/javascript">
    $(document).ready(function(){
      $.getJSON("/get-notification", function(result){
          if (result != 0) {
            $("#notification-value").text(result);
          }
          else {
            $("#notification-value").text("");
          }
      });
    });

    setInterval(function(){
      $(document).ready(function(){
        $.getJSON("/get-notification", function(result){
            if (result != 0) {
              $("#notification-value").text(result);
            }
            else {
              $("#notification-value").text("");
            }
        });
      });
    }, 10000);
  </script>
@endif
