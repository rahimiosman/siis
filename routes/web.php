<?php


Route::get('/', 'HomeController@index');
Route::get('/login', 'HomeController@login');
Route::get('/inbox', 'HomeController@inbox');
Route::get('/notification', 'HomeController@notification');
Route::get('/staff-management', 'ManagerController@staff_management');
Route::get('/inventory-management', 'ManagerController@inventory_management');
Route::get('/get-notification', 'HomeController@get_notification');
Route::get('/update-inbox/{id}', 'HomeController@read_inbox');
Route::get('/update-notification/{id}', 'HomeController@read_notification');
Route::get('/staff-list', 'StaffController@staff_list');
Route::get('/submit-request', 'StaffController@submit_request');
Route::get('/release-product', 'StaffController@release_product');
Route::get('/restock-product', 'StaffController@restock_product');
Route::get('/inventory-list', 'StaffController@inventory_list');
Route::post('/request-stock', 'StaffController@request_stock');
Route::post('/request-new-product', 'StaffController@request_new_product');
Route::post('/logout', 'HomeController@logout');
Route::post('/create-new-product', 'ManagerController@create_product');
Route::post('/login', 'HomeController@login_check');
Route::post('/release-product-process', 'StaffController@release');
Route::post('/restock-product-process', 'StaffController@restock');
Route::delete('/delete-staff/{id}/{fullname}/{location}', 'ManagerController@delete_staff');
Route::post('/update-staff', 'ManagerController@update_staff');
Route::post('/create-staff', 'ManagerController@create_staff');
