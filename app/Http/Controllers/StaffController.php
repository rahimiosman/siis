<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers;
use App\Staff;
use App\Product;
use App\Inbox;
use App\Notification;

class StaffController extends Controller
{
    public function staff_list() {
      $location = session('location');
      $staff_list = Staff::all()->where('location', $location);

      return view(
        'staff.staff_list',
        compact('staff_list')
      );
    }

    public function inventory_list() {
      $location = session('location');
      $products = Product::all()->where('product_location', $location);

      return view(
        'staff.inventory_list',
        compact('products')
      );
    }

    public function submit_request() {
      $products = Product::all()->where('product_location', session('location'));

      return view(
        'staff.submit_request',
        compact('products')
      );
    }

    public function request_stock(Request $req) {
      $req->validate([
        'product-name' => 'required',
        'message' => 'required',
        'quantity' => 'required'
      ]);

      $product_name = Helpers::raw($req->input('product-name'));
      $message = $req->input('message');
      $quantity = $req->input('quantity');
      $type = 'restock';
      $location = session('location');

      Inbox::create([
        'product_name' => $product_name,
        'type' => $type,
        'message' => $message,
        'quantity' => $quantity,
        'location' => $location,
        'read_status' => 0
      ]);

      return back()->with(['success' => 'Message successfully sent.']);

    }

    public function request_new_product(Request $req) {
      $req->validate([
        'product-name' => 'required',
        'message' => 'required',
        'quantity' => 'required'
      ]);

      $product_name = Helpers::raw($req->input('product-name'));
      $message = $req->input('message');
      $quantity = $req->input('quantity');
      $type = 'request';
      $location = session('location');

      Inbox::create([
        'product_name' => $product_name,
        'type' => $type,
        'message' => $message,
        'quantity' => $quantity,
        'location' => $location,
        'read_status' => 0
      ]);

      return back()->with(['success' => 'Message successfully sent.']);
    }

    public function release_product() {
      $products = Product::all()->where('product_location', session('location'));

      return view(
        'staff.release_product',
        compact('products')
      );
    }

    public function restock_product() {
      $products = Product::all()->where('product_location', session('location'));

      return view(
        'staff.restock_product',
        compact('products')
      );
    }

    public function release(Request $req) {
      $req->validate([
        'product-name' => 'required',
        'product-type' => 'required',
        'quantity' => 'required'
      ]);

      $product_name = Helpers::raw($req->input('product-name'));
      $product_type = Helpers::raw($req->input('product-type'));
      $quantity = $req->input('quantity');
      $location = session('location');

      $product = Product::where('product_name', $product_name)->where('product_type', $product_type)->where('product_location', $location)->first();
      $new_current_quantity = $product->product_current_quantity - $quantity;
      $max_quantity = $product->product_max_quantity;

      $check_percentage = (($product->product_current_quantity - $quantity) / $product->product_max_quantity) * 100;

      $update = [
        'product_current_quantity' => $new_current_quantity
      ];
      Product::where('product_name', $product_name)
      ->where('product_type', $product_type)
      ->where('product_location', $location)
      ->update($update);

      if($check_percentage < 20) {
        Notification::create_notification($product_name, $product_type, $new_current_quantity, $max_quantity, $location);
      }

      return back()->with(['success' => 'Successfully update the database.']);
    }

    public function restock(Request $req) {
      $req->validate([
        'product-name' => 'required',
        'product-type' => 'required',
        'quantity' => 'required'
      ]);

      $product_name = Helpers::raw($req->input('product-name'));
      $product_type = Helpers::raw($req->input('product-type'));
      $quantity = $req->input('quantity');
      $location = session('location');

      $product = Product::where('product_name', $product_name)->where('product_type', $product_type)->where('product_location', $location)->first();
      $new_current_quantity = $product->product_current_quantity + $quantity;

      $update = [
        'product_current_quantity' => $new_current_quantity
      ];
      Product::where('product_name', $product_name)
      ->where('product_type', $product_type)
      ->where('product_location', $location)
      ->update($update);

      return back()->with(['success' => 'Successfully update the database.']);
    }
}
