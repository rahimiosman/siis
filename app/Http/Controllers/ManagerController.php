<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Staff;
use App\Http\Helpers;

class ManagerController extends Controller
{
    public function staff_management() {
      $staffs = Staff::all();

      return view(
        'manager.staff_management',
        compact('staffs')
      );
    }

    public function inventory_management() {
      $products = Product::all();
      return view(
        'manager.inventory_management',
        compact('products')
      );
    }

    public function create_product(Request $req) {
      $req->validate([
        'product-name' => 'required',
        'product-type' => 'required',
        'product-location' => 'required',
        'product-max-quantity' => 'required'
      ]);

      $product_name = Helpers::raw($req->input('product-name'));
      $product_type = Helpers::raw($req->input('product-type'));
      $product_location = Helpers::raw($req->input('product-location'));
      $product_max_quantity = $req->input('product-max-quantity');
      $product_current_quantity = $req->input('product-max-quantity');

      Product::create([
        'product_name' => $product_name,
        'product_type' => $product_type,
        'product_location' => $product_location,
        'product_max_quantity' => $product_max_quantity,
        'product_current_quantity' => $product_current_quantity
      ]);

      return back()->with(['success' => 'You have successfully created a new product']);
    }

    public function create_staff(Request $req) {
      $req->validate([
        'name' => 'required',
        'phone-number' => 'required',
        'address' => 'required',
        'position' => 'required',
        'location' => 'required',
        'username' => 'required',
        'password' => 'required'
      ]);

      $fullname = Helpers::raw($req->input('name'));
      $phone_number = $req->input('phone-number');
      $address = $req->input('address');
      $position = Helpers::raw($req->input('position'));
      $location = Helpers::raw($req->input('location'));
      $username = $req->input('username');
      $password = $req->input('password');

      Staff::create([
        'fullname' => $fullname,
        'phone_number' => $phone_number,
        'address' => $address,
        'position' => $position,
        'location' => $location,
        'username' => $username,
        'password' => $password
      ]);

      return back()->with(['success' => 'You have successfully added new staff.']);
    }

    public function update_staff(Request $req) {
      $req->validate([
        'name' => 'required',
        'phone-number' => 'required',
        'address' => 'required',
        'position' => 'required',
        'location' => 'required',
        'username' => 'required',
        'password' => 'required'
      ]);

      $old_fullname = Helpers::raw($req->input('old-name'));
      $old_location = $req->input('old-location');
      $old_phone_number = $req->input('old-phone-number');
      $fullname = Helpers::raw($req->input('name'));
      $phone_number = $req->input('phone-number');
      $address = $req->input('address');
      $position = Helpers::raw($req->input('position'));
      $location = Helpers::raw($req->input('location'));
      $username = $req->input('username');
      $password = $req->input('password');

      $update = [
        'fullname' => $fullname,
        'phone_number' => $phone_number,
        'address' => $address,
        'position' => $position,
        'location' => $location,
        'username' => $username,
        'password' => $password
      ];

      Staff::where('fullname', $old_fullname)->where('location', $old_location)->where('phone_number', $old_phone_number)->update($update);

      return back()->with(['success' => 'You have successfully updated staff info.']);
    }

    public function delete_staff($id, $fullname, $location) {
      Staff::where('id', $id)->where('fullname', $fullname)->where('location', $location)->delete();

      return back()->with(['success' => 'You have successfully deleted a staff.']);
    }
}
