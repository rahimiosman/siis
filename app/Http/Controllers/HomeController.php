<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manager;
use App\Staff;
use App\Inbox;
use App\Notification;
use App\Clinic;
use App\Product;

class HomeController extends Controller
{
    public function index() {

      if (session('login?')) {
        $products = Product::all();
        $location = Clinic::all();

        return view(
          'home.index',
          compact('products', 'location')
        );
      }

      return redirect()->action('HomeController@login');

    }

    public function login() {


      return view(
        'home.login'
      );
    }

    public function login_check(Request $req) {
      $req->validate([
        'username' => 'required',
        'password' => 'required'
      ]);

      $username = $req->input('username');
      $password = $req->input('password');

      if (Manager::where('username', $username)->where('password', $password)->count() > 0) {
        session([
          'login?' => true,
          'position' => 'manager',
          'username' => $username
        ]);
        return redirect()->action('HomeController@index');
      }

      else if (Staff::where('username', $username)->where('password', $password)->count() > 0) {
        $staff = Staff::where('username', $username)->where('password', $password)->first();
        if ($staff->position == 'clerk')
        session([
          'login?' => true,
          'position' => $staff->position,
          'username' => $username,
          'location' => $staff->location,
          'address' => $staff->address
        ]);
        return redirect()->action('HomeController@index');
      }

      return back()->withErrors('Wrong username or password');
    }

    public function logout() {
      auth()->logout();
      session()->flush();
      return redirect()->action('HomeController@index');
    }

    public function inbox() {
      $inboxes = Inbox::all();

      return view(
        'home.inbox',
        compact('inboxes')
      );
    }

    public function notification() {
      $notifications = Notification::all();

      return view(
        'home.notification',
        compact('notifications')
      );
    }

    public function read_inbox($id) {
      $update = ['read_status' => 1];
      Inbox::where('id', $id)->update($update);
      return back();
    }

    public function read_notification($id) {
      $update = ['read_status' => 1];
      Notification::where('id', $id)->update($update);
      return back();
    }

    public function get_notification() {

      return json_encode(Notification::where('read_status', 0)->count());
    }
}
