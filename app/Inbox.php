<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inbox extends Model
{
    protected $fillable = ['product_name', 'type', 'message', 'quantity', 'location', 'read_status'];

    static function get_inbox_value() {
      $value = Inbox::where('read_status', 0)->count();

      if ($value != 0) {
        return $value;
      }
      else {
        return '';
      }
    }
}
