<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
  protected $fillable = ['product_name', 'product_type', 'current_quantity', 'max_quantity', 'location', 'read_status'];

  public static function create_notification($product_name, $product_type, $current_quantity, $max_quantity, $location) {

    Notification::create([
      'product_name' => $product_name,
      'product_type' => $product_type,
      'current_quantity' => $current_quantity,
      'max_quantity' => $max_quantity,
      'location' => $location,
      'read_status' => 0
    ]);

  }
}
