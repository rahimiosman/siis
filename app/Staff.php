<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $fillable = ['fullname', 'phone_number', 'address', 'position', 'location', 'username', 'password'];
}
