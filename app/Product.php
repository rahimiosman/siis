<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['product_name', 'product_type', 'product_location', 'product_max_quantity', 'product_current_quantity'];
}
